import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:inventario_proyecto_aplicacion/preferencias_usuario/preferencias_usuario.dart';

class UsuarioProvider {

  final String _firebaseToken = 'AIzaSyD7SSt6zFlAR52pUmZwEK_M66kYgD-NXqI';
  final _prefs = new PreferenciasUsuario();


  Future<bool> login( String email, String password) async {

    final authData = {
      'email'    : email,
      'password' : password,
      'returnSecureToken' : true
    };

    final resp = await http.post(
      'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=$_firebaseToken',
      body: json.encode( authData )
    );

    Map<String, dynamic> decodedResp = json.decode( resp.body );

    print(decodedResp);

   //decodedResp.containsKey('idToken')
    if (decodedResp.containsKey('idToken') ) {
      
      //_prefs.email = decodedResp['email'];
      //_prefs.token = decodedResp['idToken'];
      
      return true;  //{ 'ok': true, 'token': decodedResp['idToken'] };
    } else {
      return false;  //{ 'ok': true, 'token': decodedResp['idToken'] };
    }

  }


  Future<bool> nuevoUsuario( String email, String password ) async {

    final authData = {
      'email'    : email,
      'password' : password,
      'returnSecureToken' : true
    };

    final resp = await http.post(
      'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=$_firebaseToken',
      body: json.encode( authData )
    );

    Map<String, dynamic> decodedResp = json.decode( resp.body );

    print(decodedResp);

    if ( decodedResp.containsKey('idToken') ) {
      
      _prefs.token = decodedResp['idToken'];
      _prefs.email = authData['email'];


      return true;  //{ 'ok': true, 'token': decodedResp['idToken'] };
    } else {
      return false;  //{ 'ok': false, 'mensaje': decodedResp['error']['message'] };
    }


  }



}