
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:inventario_proyecto_aplicacion/models/cambios_model.dart';

import 'package:inventario_proyecto_aplicacion/models/producto.dart';
import 'package:inventario_proyecto_aplicacion/providers/productos_provider.dart';

class ProductosServices with ChangeNotifier{

  List<Producto> _productos;
  List<CambiosModel> _cambios;

  final productosProviders = new ProductosProvider();

  List<Producto> get productos => this._productos;
  bool get existeProductos => this._productos != null ? true : false;

  List<CambiosModel> get cambios => this._cambios;
  bool get existeCambios => this._cambios != null ? true : false;

  set productoList(List<Producto> p){
    this._productos =  p;
    notifyListeners();
  }

  cargarListProductos()async{
    productoList = await cargarProductos() ;
  }

  Future<List<Producto>> cargarProductos()async{
    List<Producto> productos = await productosProviders.cargarProductos();
    return productos;
  }


  void agregarProducto( Producto producto , int cantidad ) async {
    await productosProviders.crearProducto(producto, cantidad);
    this._productos.add(producto);
    notifyListeners();
  }

  Future<String> subirFoto( File foto ) async {
    final fotoUrl = await productosProviders.subirImagen(foto);

    return fotoUrl;
  }


  void editarProducto( Producto producto, int cantidad ) async {
    await productosProviders.editarProducto(producto, cantidad);
    notifyListeners();
  }

  void borrarProducto( String id ) async {
    await productosProviders.borrarProducto(id);
    notifyListeners();
  }

  
  set cambiosProductoList(List<CambiosModel> cambios){
    this._cambios = cambios;
    notifyListeners();
  }

  cargarCambiosProducto()async{
    cambiosProductoList = await cargarListCambios() ;
  }

  Future<List<CambiosModel>> cargarListCambios()async{
    List<CambiosModel> productos = await productosProviders.cargarCambios();
    return productos;
  }



}