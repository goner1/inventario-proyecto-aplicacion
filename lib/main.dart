
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:inventario_proyecto_aplicacion/estado/productos_services.dart';
import 'package:inventario_proyecto_aplicacion/pages/add_producto_page.dart';
import 'package:inventario_proyecto_aplicacion/pages/login_page.dart';
import 'package:inventario_proyecto_aplicacion/pages/productos_page.dart';
import 'package:inventario_proyecto_aplicacion/preferencias_usuario/preferencias_usuario.dart';
import 'package:inventario_proyecto_aplicacion/pages/historial_producto_page.dart';

 
void main() async { 
    WidgetsFlutterBinding.ensureInitialized();
    final prefs = new PreferenciasUsuario();
    await prefs.initPrefs();
    
    runApp(MyApp());
  }
  
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: ( _ ) => new ProductosServices()),
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Material App',
          initialRoute: 'login',
          routes: {
            'login' : ( _ ) => LoginPage(),
            'productos' : ( _ ) => Productos(),
            'addProducto' : ( _ ) => AddProductoPage(),
            'logCambios' : ( _ ) => HistorialCambios()
          }
      ),
    );
  }
}