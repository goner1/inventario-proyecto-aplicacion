
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

import 'package:inventario_proyecto_aplicacion/providers/productos_provider.dart';
import 'package:inventario_proyecto_aplicacion/models/producto.dart';

//Utilizacion de la libreria mokito para la utilizacion de objetos ficticios en algunos casos
class MockProductosProvider extends Mock implements ProductosProvider{}
void main(){

  group("Subir, editar y editar producto sin mockup", (){
    //Given
    Producto producto = new Producto(
      id: "-MNHd1jxwce_vE3i-e4J",
      nombre: "Mouse",
      cantidad: 4,
      descripcion: "fddsf",
      categoria: "fgdfg",
      fotoUrl: ""
    );
    //when
    ProductosProvider prod = new ProductosProvider();

    test("Crear Producto", ()async{
    //then
      expect(await prod.crearProducto(producto, producto.cantidad), true);
    });

    test("Editar Producto", ()async{
      //then
      expect(await prod.editarProducto(producto, 4), true);
    });

    test("Registro cambios", (){
      //then
      prod.crearCambios(producto, "Añadido al inventario", 4);
    });

  });

  group("Subir, editar y editar producto con mockup", (){
    //Given
    Producto producto = new Producto(
      id: "-MNGhsDqnNRucR6GtKOj",
      nombre: "Mouse",
      cantidad: 6,
      descripcion: "fddsf",
      categoria: "fgdfg",
      fotoUrl: ""
    );
    //when
    MockProductosProvider prod = new MockProductosProvider();
    when(prod.crearProducto(producto, 6)).thenAnswer((realInvocation) => Future.value(true));
    when(prod.editarProducto(producto, 4)).thenAnswer((realInvocation) => Future.value(true));
    when(prod.crearCambios(producto, "Añadir al inventario",4)).thenAnswer((realInvocation) => Future.value(true));

    test("Crear Producto", ()async{
    //then
      expect(await prod.crearProducto(producto, producto.cantidad), true);
    });

    test("Editar Producto", ()async{
      //then
      expect(await prod.editarProducto(producto, 4), true);
    });

    test("Registro cambios", (){
      //then
      prod.crearCambios(producto, "Añadido al inventario", 4);
    });

  });

}

