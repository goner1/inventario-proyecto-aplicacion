
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import 'package:inventario_proyecto_aplicacion/estado/productos_services.dart';
import 'package:inventario_proyecto_aplicacion/models/producto.dart';
import 'package:inventario_proyecto_aplicacion/providers/productos_provider.dart';
import 'package:inventario_proyecto_aplicacion/utils/utils.dart' as utils;


class AddProductoPage extends StatefulWidget {

  @override
  _AddProductoPageState createState() => _AddProductoPageState();
}

class _AddProductoPageState extends State<AddProductoPage> {

  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final productoProvider = new ProductosProvider();

  Producto producto = new Producto();
  bool _guardando = false;
  File foto;
  int cantidad = 0; 


  @override
  Widget build(BuildContext context) {
    
    final Producto prodData = ModalRoute.of(context).settings.arguments;
    bool existe = false;
    
    if ( prodData != null){
      producto = prodData;
      existe = true;
      cantidad = producto.cantidad;
    }

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text("Producto"),
        actions: <Widget>[
          (existe)?
          IconButton(
            icon:Icon(Icons.photo_size_select_actual,),
            onPressed: (){},):
          IconButton(
            icon: Icon(Icons.photo_size_select_actual), 
            onPressed: _seleccionarFoto,
            ),
          (existe)?
          IconButton(
            icon:Icon(Icons.camera_alt,),
            onPressed: (){},):
          IconButton(
            icon: Icon(Icons.camera_alt), 
            onPressed: _tomarFoto,
            )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                _mostrarFoto(),
                _crearNombre(existe),
                _crearCantidad(),
                _crearCategoria(existe),
                _crearDescripcion(existe),
                Padding(padding: EdgeInsets.only(top: 10.0)),
                _crearBoton(),
                (existe) ? _botonDetalles() : Container()
              ],
            )
          ),
        )
      ),
    );


  }

  

  Widget _crearNombre(bool existe){
    
    return TextFormField(
      initialValue: producto.nombre,
      enabled: (existe)?false:true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: 'Producto'
      ),
      onSaved: (value) => producto.nombre= value,
      validator: (value) {
        if ( value.length < 3){
          return 'Ingrese el nombre del producto';
        }else{
          return null;
        }
      },
    );
  }

  Widget _crearDescripcion(bool existe){
    
    return TextFormField(
      initialValue: producto.descripcion,
      enabled: (existe)?false:true,
      textCapitalization: TextCapitalization.sentences,
      maxLines: 3,
      decoration: InputDecoration(
        labelText: 'Descripción'
      ),
      onSaved: (value) => producto.descripcion = value,
      validator: (value) {
        if ( value.length < 5){
          return 'Ingrese una descripción del producto';
        }else{
          return null;
        }
      },
    );
  }

  Widget _crearCantidad(){
    return TextFormField(
      initialValue: cantidad.toString(),
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: 'Cantidad'
      ),
      onSaved: (value) => producto.cantidad = int.parse(value),
      validator: (value) {

        if ( utils.isNumeric(value) && int.parse(value) >= 0 ){
          return null;
        }else{
          return 'Solo numeros';
        }
        
      },
    );
  }

  Widget _crearCategoria(bool existe){
    
    return TextFormField(
      initialValue: producto.categoria,
      enabled: (existe)?false:true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: 'Categoria'
      ),
      onSaved: (value) => producto.categoria = value,
      validator: (value) {
        if ( value.length < 3){
          return 'Ingrese el nombre del producto';
        }else{
          return null;
        }
      },
    );
  }

  Widget _botonDetalles(){
    return Container(
      width: 130,
      child: RaisedButton.icon(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        color: Colors.blueAccent,
        textColor: Colors.white,
        label: Text("Cambios"),
        icon: Icon(Icons.receipt_long),
        onPressed: () => Navigator.pushNamed(context, 'logCambios', arguments: producto.id),
      ),
    );
  }


  Widget _crearBoton(){
    return Container(
          width: 130,
          child: RaisedButton.icon(
        
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        color: Colors.blueAccent ,
        textColor: Colors.white,
        label: Text("Guardar"),
        icon: Icon(Icons.save),
        onPressed:(_guardando) ? null : _submit,
      ),
    );
  }

  void _submit() async{
    
    final productosServices =  Provider.of<ProductosServices>(context, listen: false); 

    if( !formKey.currentState.validate() ) return;

    formKey.currentState.save();

    setState(() {
      _guardando = true;
    });

    if( foto != null){
      producto.fotoUrl = await productoProvider.subirImagen(foto);
    }

    if( producto.id == null){
      productosServices.agregarProducto(producto, cantidad);
    }else{
      productosServices.editarProducto(producto, cantidad);
    }
    setState(() {

    });

    mostrarSnackbar('Registro editado');
  
    Navigator.pop(context);
    
  }


  void mostrarSnackbar(String mensaje){

    final snackbar = SnackBar(
      content: Text(mensaje),
      duration: Duration ( milliseconds: 1500),
      );

      scaffoldKey.currentState.showSnackBar(snackbar);

  }

  Widget _mostrarFoto(){

    
    if (producto.fotoUrl != null) {
 
      return FadeInImage(
        image: NetworkImage( producto.fotoUrl ),
        placeholder: AssetImage('assets/jar-loading.gif'),
        height: 300.0,
        fit: BoxFit.contain,
      );
 
    } else {
 
      if( foto != null ){
        return Image.file(
          foto,
          fit: BoxFit.cover,
          height: 300.0,
        );
      }
      return Image.asset('assets/inventario.png');
    }
  }

  _seleccionarFoto() async {
     
    _procesarImagen(ImageSource.gallery);

  }

  _tomarFoto() async {
    
    _procesarImagen(ImageSource.camera);

  }

  _procesarImagen( ImageSource origen ) async{

      foto = await ImagePicker.pickImage(
      source: origen
    );

    if( foto != null){
      producto.fotoUrl = null;
    }

    setState(() {});
  }
}

