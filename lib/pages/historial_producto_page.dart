import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:inventario_proyecto_aplicacion/estado/productos_services.dart';
import 'package:inventario_proyecto_aplicacion/models/cambios_model.dart';

class HistorialCambios extends StatefulWidget {
  @override
  _HistorialCambiosState createState() => _HistorialCambiosState();
}

class _HistorialCambiosState extends State<HistorialCambios> {
  String id = '';

   @override
  Widget build(BuildContext context) {
    final productosServices =  Provider.of<ProductosServices>(context, listen: true); 
    productosServices.cargarCambiosProducto();


    final String prodData = ModalRoute.of(context).settings.arguments;
    id = prodData;
    return Scaffold(
      appBar: AppBar(
        title: Text('Movimientos'),
        centerTitle: true,
      ),
      body:productosServices.existeCambios ? ListView.builder(
              itemCount: productosServices.cambios.length,
              itemBuilder: (context, i) => (productosServices.cambios[i].idProducto == id) ? _crearItem(context, productosServices.cambios[i]) :
                Container()
      ) : Center( child: CircularProgressIndicator()),
    );
  }

  Widget _crearItem(BuildContext context, CambiosModel logCambio){

      return Card(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text('Producto =>  ${logCambio.producto}\n'+
                                  'Acción     =>  ${ logCambio.accion }\n'+
                                  'Usuario    => ${logCambio.usuario}'),
                      subtitle: Text( logCambio.fecha ),
                      //onTap: () => Navigator.pushNamed(context, 'producto', arguments: producto ),
                    ),

                  ],
                ),
              
      );
  }
}