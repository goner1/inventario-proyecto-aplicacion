import 'dart:convert';

Producto productoFromJson(String str) => Producto.fromJson(json.decode(str));

String productoToJson(Producto data) => json.encode(data.toJson());

class Producto {
    Producto({
        this.id,
        this.nombre,
        this.descripcion,
        this.cantidad,
        this.categoria,
        this.fotoUrl,
    });

    String id;
    String nombre;
    String descripcion;
    int cantidad;
    String categoria;
    String fotoUrl;

    factory Producto.fromJson(Map<String, dynamic> json) => Producto(
        id: json["id"],
        nombre: json["nombre"],
        descripcion: json["descripcion"],
        cantidad: json["cantidad"],
        categoria: json["categoria"],
        fotoUrl: json["fotoUrl"],
    );

    Map<String, dynamic> toJson() => {
        //"id": id,
        "nombre": nombre,
        "descripcion": descripcion,
        "cantidad": cantidad,
        "categoria": categoria,
        "fotoUrl": fotoUrl,
    };
}
