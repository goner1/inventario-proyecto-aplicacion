import 'dart:convert';

CambiosModel cambiosModelFromJson(String str) => CambiosModel.fromJson(json.decode(str));

String cambiosModelToJson(CambiosModel data) => json.encode(data.toJson());

class CambiosModel {
    String id;
    String producto;
    String usuario;
    String accion;
    int cantidad;
    String fecha;
    String idProducto;

    CambiosModel({
        this.id,
        this.producto,
        this.usuario,
        this.accion,
        this.cantidad,
        this.fecha,
        this.idProducto
    });

    factory CambiosModel.fromJson(Map<String, dynamic> json) => CambiosModel(
        id: json["id"],
        producto: json["producto"],
        usuario: json["usuario"],
        accion: json["accion"],
        cantidad: json["cantidad"],
        fecha: json["fecha"],
        idProducto: json["idProducto"],
    );

    Map<String, dynamic> toJson() => {
        "producto": producto,
        "usuario": usuario,
        "accion": accion,
        "cantidad": cantidad,
        "fecha": fecha,
        "idProducto": idProducto
    };
}
