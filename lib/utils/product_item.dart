import 'package:flutter/material.dart';


class CustomProducto extends StatelessWidget {
  CustomProducto({
    Key key,
    this.image,
    this.title,
    this.subtitle,
    this.cantidad,
    //this.fecha,
  }) : super(key: key);

  final Widget image;
  final String title;
  final String subtitle;
  final String cantidad;

  //final String fecha;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: SizedBox(
        height:90,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 1.0,
              child: image,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20.0, 0.0, 2.0, 0.0),
                child: _ProductDescription(
                  title: title,
                  subtitle: subtitle,
                  cantidad: cantidad,
                ),
              ),
            )
          ],
        ),
        
      ),
      
    );
  }
}

class _ProductDescription extends StatelessWidget {
  _ProductDescription({
    Key key,
    this.title,
    this.subtitle,
    this.cantidad,
    //this.fecha,
  }) : super(key: key);

  final String title;
  final String subtitle;
  final String cantidad;
  //final String fecha;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Padding(
                    padding: EdgeInsets.only(top:1),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          '$title',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(bottom: 2.0)),
                        Text(
                          '$subtitle',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            fontSize: 12.0,
                            color: Colors.black54,
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                'cantidad: $cantidad',
                                style: const TextStyle(
                                  fontSize: 12.0,
                                  color: Colors.black87,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
          ),
        ),
      ],
    );
  }
}


