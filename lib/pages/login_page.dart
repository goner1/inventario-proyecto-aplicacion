import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:inventario_proyecto_aplicacion/estado/login_bloc.dart';
import 'package:inventario_proyecto_aplicacion/providers/usuario_provider.dart';
import 'package:inventario_proyecto_aplicacion/utils/utils.dart';

class LoginPage extends StatelessWidget {
  
  bool existe = false;

  @override
  Widget build(BuildContext context) {
    
    final loginBloc = LoginBloc();
    final bool prodData = ModalRoute.of(context).settings.arguments;
    

    if ( prodData != null){
      existe = true;
    }
    return Scaffold(
      body: SingleChildScrollView(
          child: Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              SizedBox(height: 80,),
              Container(
                margin: EdgeInsets.only(right: 15),
                child: Image.asset("assets/inventario.png", width: 140,
                )
              ),
              correo( loginBloc ),
              pass( loginBloc ),
              SizedBox(height: 28,),
              botonIngresar( loginBloc ),

              Container(
                margin: EdgeInsets.only(top: 20),
                child: Column(
                  children: [
                    Text("¿No tienes cuenta?", style: TextStyle(
                      fontFamily: 'OpenSans',
                      fontSize: 16.6,
                    ),),
                    SizedBox(height: 8,),
                    InkWell(
                      child: Text("Crear Ahora", style: TextStyle(
                        fontFamily: 'OpenSans',
                        fontSize: 16.6,
                        fontWeight: FontWeight.bold
                      ),),
                      onTap: (){},
                    )
                  ],
                ),
              )
              
            ],
          ),
        ),
      ),
    );
  }

  Widget correo(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (context, snapshot) {
        return Container(
                margin: EdgeInsets.only(left: 50, right: 50, top: 58),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(
                    color: Colors.black,
                    width: 1,
                  )
                ),
                child: TextField(
                  keyboardType: TextInputType.emailAddress,
                  style: TextStyle(
                    fontStyle: FontStyle.italic
                  ),
                  decoration: InputDecoration(
                    labelText: "Ingresa un correo",
                    suffixIcon: Icon(Icons.email_outlined),
                    contentPadding: EdgeInsets.fromLTRB(30.0, 0, 0, 0),
                    border: InputBorder.none,
                    //errorText: snapshot.error,
                    
                  ),
                  onChanged: bloc.changeEmail,
                ),
              );
      }
    );
  }

  Widget pass(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (context, snapshot) {
        return Container(
                margin: EdgeInsets.only(left: 50, right: 50, top: 28),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(
                    color: Colors.black,
                    width: 1,
                  )
                ),
                child: TextField(
                  obscureText: true,
                  style: TextStyle(
                    fontStyle: FontStyle.italic
                  ),
                  decoration: InputDecoration(
                    labelText: "Ingresa tu contraseña",
                    suffixIcon: Icon(Icons.visibility_off_outlined),
                    contentPadding: EdgeInsets.fromLTRB(30.0, 0, 0, 0),
                    border: InputBorder.none,
                    errorText: snapshot.error
                  ),
                  onChanged: bloc.changePassword,
                  onTap: () {
                    existe = false;
                  },
                ),
              );
      }
    );
  }

  
  Widget botonIngresar(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (context, snapshot) {
        return RaisedButton(
                  color: Color.fromRGBO(88, 210, 206, 82),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                    side: BorderSide(color: Colors.transparent,)
                  ),
                  child: Container(
                    height: 46,
                    width: 228,
                    alignment: Alignment.center,
                    child: Text("Ingresar", style: TextStyle(
                      color: Colors.black, 
                      fontSize: 16,
                      fontWeight: FontWeight.w400 
                    ),)
                  ),
                  onPressed: snapshot.hasData  ? ()=> _login(bloc, context) : null
                );
                
      }
    );
  }

  _login(LoginBloc bloc, BuildContext context) async{
    final usuarioProvider = new UsuarioProvider();
    
    bool info = await usuarioProvider.login(bloc.email, bloc.password);


    if( info ){
      Navigator.pushReplacementNamed(context, 'productos');
    }else{
      mostrarAlerta(context, 'La contraseña o su correo es incorrecto por favor revise sus credenciales.');
    

  }


 }
}