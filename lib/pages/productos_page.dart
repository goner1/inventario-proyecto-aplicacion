
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:inventario_proyecto_aplicacion/estado/productos_services.dart';
import 'package:inventario_proyecto_aplicacion/models/producto.dart';
import 'package:inventario_proyecto_aplicacion/utils/product_item.dart';


class Productos extends StatelessWidget {
  
   @override
  Widget build(BuildContext context) {
    final productosServices =  Provider.of<ProductosServices>(context, listen: true); 
    productosServices.cargarListProductos();
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Productos"),
          actions: [
            IconButton(
              icon: Icon(Icons.exit_to_app), 
              onPressed: () => Navigator.pushNamedAndRemoveUntil(context, 'login', (route) => false, arguments: true)
              
            )
          ],
        ),
        body: productosServices.existeProductos ? Container(
                margin: EdgeInsets.all(6),
                child:  ListView.builder(
                    itemCount: productosServices.productos.length,
                    itemBuilder:  (context,  i) => productoTile(productosServices.productos[i], context) 
                )) : Center( child: CircularProgressIndicator()),
       floatingActionButton: FloatingActionButton(
         child: Icon(Icons.add),
         onPressed: () => Navigator.pushNamed(context, 'addProducto'),
       ),
    );
  }

  Widget productoTile(Producto producto, BuildContext context){
    return  Container(
      height: 90,
      child: Card(
        child: InkWell(
          child: CustomProducto(
            image: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(left: 4),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                image: DecorationImage(
                  image: imagen(producto.fotoUrl)
                )
              ),
            ),
            title: producto.nombre,
            subtitle: producto.descripcion,
            cantidad: producto.cantidad.toString(),
          ),
          onTap: () => Navigator.pushNamed(context, 'addProducto', arguments: producto),
        ),
      ),
    );
  }

  imagen(fotoUrl){
    if (fotoUrl == null) {
      return AssetImage('assets/inventario.png') ;
    } else {
      return NetworkImage( fotoUrl);
    }
  }
}