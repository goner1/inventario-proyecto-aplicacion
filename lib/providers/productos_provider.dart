
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mime_type/mime_type.dart';
import 'package:date_format/date_format.dart';

import 'package:inventario_proyecto_aplicacion/preferencias_usuario/preferencias_usuario.dart';
import 'package:inventario_proyecto_aplicacion/models/cambios_model.dart';
import 'package:inventario_proyecto_aplicacion/models/producto.dart';

class ProductosProvider {



  final String _url = 'https://flutter-varios-2f2ac.firebaseio.com';
  final _prefs = new PreferenciasUsuario();
  CambiosModel _cambios = new CambiosModel();

  Future<bool> crearProducto( Producto producto , int cantidad) async {

    //'$_url/productos.json?auth=${ _prefs.token }';
    final url = '$_url/productos.json';

    final resp = await http.post( url, body: productoToJson(producto));

    final decodedData = json.decode(resp.body);

    producto.id = decodedData['name'];

    crearCambios(producto, 'Añadido al inventario', cantidad);

    return true;

  }


  Future<bool> editarProducto( Producto producto, int cantidad ) async {
    print(producto.id);

    //'$_url/productos/${ producto.id }.json?auth=${ _prefs.token }';
    final url = '$_url/productos/${ producto.id }.json';

    final resp = await http.put( url, body: productoToJson(producto) );

    final decodedData = json.decode(resp.body);

    crearCambios(producto, 'Cantidad Modificada', cantidad);

    print( decodedData );

    if (decodedData != null) {
      return true;
    }else{
      return false;
    }

  }



  Future<List<Producto>> cargarProductos() async {

    final url  = '$_url/productos.json?auth=${ _prefs.token }';
    final resp = await http.get(url);

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    final List<Producto> productos = new List();


    if ( decodedData == null ) return [];
    
    if ( decodedData['error'] != null ) return [];


    decodedData.forEach( ( id, prod ){

      final prodTemp = Producto.fromJson(prod);
      prodTemp.id = id;

      productos.add( prodTemp );

    });

    return productos;

  }


  Future<int> borrarProducto( String id ) async { 

    final url  = '$_url/productos/$id.json?auth=${ _prefs.token }';
    final resp = await http.delete(url);

    print( resp.body );

    return 1;
  }


  Future<String> subirImagen( File imagen ) async {

    final url = Uri.parse('https://api.cloudinary.com/v1_1/dgpolh4ws/image/upload?upload_preset=c5ujgm6r');
    final mimeType = mime(imagen.path).split('/'); //image/jpeg

    final imageUploadRequest = http.MultipartRequest(
      'POST',
      url
    );

    final file = await http.MultipartFile.fromPath(
      'file', 
      imagen.path,
      contentType: MediaType( mimeType[0], mimeType[1] )
    );

    imageUploadRequest.files.add(file);


    final streamResponse = await imageUploadRequest.send();
    final resp = await http.Response.fromStream(streamResponse);

    if ( resp.statusCode != 200 && resp.statusCode != 201 ) {
      print('Algo salio mal');
      print( resp.body );
      return null;
    }

    final respData = json.decode(resp.body);
    //print( respData);

    return respData['secure_url'];


  }

  Future<bool> crearCambios(Producto producto, String accion, int cantidad) async {
    
     _cambios.producto = producto.nombre;
     _cambios.cantidad = producto.cantidad;
     _cambios.usuario = "o.leiva01@ufromail.cl"; //_prefs.email;
     _cambios.idProducto = producto.id;

     if(accion == "Añadido al inventario"){
       _cambios.accion = accion;
     }else{
       _cambios.accion = accion + " de $cantidad a ${producto.cantidad}";
     }
     _cambios.fecha = formatDate(new DateTime.now(), [yyyy,'-', mm ,'-', dd, '-', HH, ':', nn, ':', ss]);

      //'$_url/logCambios.json?auth=${ _prefs.token }';
     final url = '$_url/logCambios.json';

     
     final resp = await  http.post( url, body: cambiosModelToJson(_cambios));
     final decodedData = json.decode(resp.body);
     
     return true;

   }

    
   Future<List<CambiosModel>> cargarCambios() async{

    final url = '$_url/logCambios.json?auth=${ _prefs.token }';
    final resp = await http.get(url);
    int cont = 0;

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    final List<CambiosModel> cambios = new List();
    final List<CambiosModel> cambios2 = new List();

    if( decodedData == null ) return[];

    decodedData.forEach((id, logCambio) { 

        final prodTemp = CambiosModel.fromJson(logCambio);
        prodTemp.id = id;

        cambios.add( prodTemp );
    });
    
    //invierto la lista para que se muestren los cambio maas recientes primero
    cambios2.length = cambios.length;    
    for (var i = cambios.length-1; i >= 0; i--) {
      cambios2[i] = cambios[cont];
      cont++;
    }

    return cambios2;
   }


}

