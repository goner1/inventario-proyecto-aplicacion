
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

import 'package:inventario_proyecto_aplicacion/providers/usuario_provider.dart';

//Utilizacion de la libreria mokito para la utilizacion de objetos ficticios en algunos casos
class MockUsuarioProvider extends Mock implements UsuarioProvider{}
void main(){

  group("Login", (){
    // Given 
    var email = "o.leiva01@ufromail.cl";
    var password = "123456";
    
    //Sin mockito
    test("Login whit firebase", ()async{
      
      //When 
      // Tuve que comentar unas lineas en las que se utiliza shared_preferences la cual tiene dependencias nativa 
      // como el test no lo estoy corriendo sobre un dispositivo me genera error al ejecutar el test
      UsuarioProvider user = new UsuarioProvider();
      // Then
      expect(await user.login(email, password), true);
    });

    //Uso de mockito
    test("Login with Mock", ()async{

    // When
    MockUsuarioProvider user = MockUsuarioProvider();
    when(user.login(email, password)).thenAnswer((realInvocation) => Future.value(true));

    //Then
    expect(await user.login(email, password), true);
    });

  });
}